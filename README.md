# CERIcompiler

Le for marche, la plupart de ce que j'ai implémenté aussi, les fonctions (procédures) aussi

dans le test.p on a par exemple for tmp=0.0 to 13 et en affichage on a de 1 a 14 ce qui fait bien 13 iterations (13-0=13) c'est juste que c'est decalé dans l'affichage (meme histoire pour le while)

j'ai supprimé une tres grande partie de mes cout de debuggage histoire que le code soit lisible

un seul commit car j'avais du mal avec les commandes git

j'ai voulu ajouter la possibilité d'écrire des commentaires je pense qu'il faut juste attendre la fin d'un traitement de ligne et voir si on a tel caractere ex: # et puis jump a la ligne suivante mais j'avais du mal a concevoir la chose en code

**Download the repository :**

> git clone git@framagit.org:jourlin/cericompiler.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Identifier {"," Identifier} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Identifier ":=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

