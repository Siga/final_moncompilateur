
//mateo novello l2g2

#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN, CHAR, DOUBLE};

TOKEN current;

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

//pour les fonctions (procedure)
string *fon = new string[15000];
int ind = 0;
	
map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

void Error(string s){
	cerr<<"\n";
	cerr << " !!!! lign no "<<lexer->lineno()<<"!!!!  read : '"<<lexer->YYText()<<"'("<<current<<") but !!!! :  ";
	cerr<< s << endl;
	cerr<<"\n";
	exit(-1);
}
		
TYPES Identifier(void){
	TYPES type;
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPES Number(void){
	bool decimal=false;
	double d;
	unsigned int *i;
	string nombre=lexer->YYText();
	if(nombre.find(".")!=string::npos){
		d=atof(lexer->YYText());
		i=(unsigned int *) &d;
		cout <<"\tsubq $8,%rsp"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

TYPES CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax"<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

TYPES Type(void){
	if(current!=KEYWORD)
		Error("type expected");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}else
		Error("unknown type");
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> IDs;
	enum TYPES type;
	if(current!=ID)
		Error("ID expected");
	IDs.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("id expected");
		IDs.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error(" --> ':' expected");
	current=(TOKEN) lexer->yylex();
	type=Type();
	for (set<string>::iterator i=IDs.begin(); i!=IDs.end(); i++){
	    if(type==INTEGER)
	    	cout << *i << ":\t.quad 0"<<endl;
	    else if(type==DOUBLE)
	    	cout << *i << ":\t.double 0.0"<<endl;
	    else if(type==CHAR)
	    	cout << *i << ":\t.byte 0"<<endl;
	    else 
	    	Error("type unknown");
        DeclaredVariables[*i]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex(); 
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("'.' expected");
	current=(TOKEN) lexer->yylex();
}

TYPES Expression(void);			// Called by Term() and calls Term()

TYPES Factor(void){
	TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' expected");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	    else if(current==ID)
			type = Identifier();
		else if(current==CHARCONST)
			type = CharConst();
		else
			Error("'(' / lettre / nombre expected");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPES Term(void){
	OPMUL mulop;
	TYPES ftype, stype;
	ftype = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();
		stype = Factor();
		if(ftype!=stype) Error("operandes pas meme type");
		switch(mulop){
			case AND:
				if(stype!=BOOLEAN)
					Error("type boolean expected");
				cout << "\tpop %rbx"<<endl;
				cout << "\tpop %rax"<<endl;
				cout << "\tmulq	%rbx"<<endl;
				cout << "\tpush %rax\t# AND"<<endl;
				break;
			case MUL:
				if(stype!=INTEGER&&stype!=DOUBLE)
					Error("numerique expected");
				if(stype==INTEGER){
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\tmulq	%rbx"<<endl;
					cout << "\tpush %rax\t# MUL"<<endl;
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t"<<endl; 
				}
				break;
			case DIV:
				if(stype!=INTEGER&&stype!=DOUBLE)
					Error("numerique type expected");
				if(stype==INTEGER){
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\tmovq $0, %rdx"<<endl;
					cout << "\tdiv %rbx"<<endl;
					cout << "\tpush %rax\t# DIV"<<endl;
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(stype!=INTEGER)
					Error("type non entier pour le modulo");
				cout << "\tmovq $0, %rdx"<<endl; 
				cout << "\tdiv %rbx"<<endl;
				cout << "\tpush %rdx\t# MOD"<<endl;
				break;
			default:
				Error("operateur inconnu");
		}
	}
	return ftype;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void){
	OPADD adop;
	TYPES ftype, stype;
	ftype = Term();
	while(current==ADDOP){
		adop = AdditiveOperator();		// Save operator in local variable
		stype = Term();
		if(ftype!=stype) Error("Les operandes ne sont pas du meme type");
		switch(adop){
			case OR:
				if(stype!=BOOLEAN)
					Error("booleen expected");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break;			
			case ADD:
				if(stype!=INTEGER&&stype!=DOUBLE)
					Error("numerique expected");
				if(stype==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;						
			case SUB:	
				if(stype!=INTEGER&&stype!=DOUBLE)
					Error("numerique expeced ");
				if(stype==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	
			default:
				Error("operateur inconnu");
		}
	}
	return ftype;
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPES Expression(void){
	TYPES ftype, stype;
	unsigned long long tag;
	OPREL oprel;
	ftype = SimpleExpression();
	if(current==RELOP){
		tag=++TagNumber;
		oprel = RelationalOperator();
		stype = SimpleExpression();
		if(ftype!=stype) Error("Les deux operandes ne sont pas du meme type");
		if(ftype!=DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case SUPE:
			cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case SUP:
			cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			default:
				Error("Opérateur de comparaison unknown");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return ftype;
}

void Statement(void);

//pour les for
string *var = new string[15000];
TYPES *typ = new TYPES[15000];
int cptt = 0; 

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	TYPES ftype, stype;
	string variable;
	if(current!=ID)
		Error("Identificateur expected");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	ftype = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' expecteds");
	current=(TOKEN) lexer->yylex();
	stype = Expression();
	if(ftype!=stype) Error("Les operandes ne sont pas du meme type");
	if(ftype==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl; 
	}
	else
		cout << "\tpop "<<variable<<endl;
	var[cptt]=variable;
	typ[cptt]=ftype;
	cptt++;
}

//<case label list> ::= <constant> {, <constant> }
TYPES CaseLabelList(unsigned long long tag){
	TYPES ftype,stype;
	current=(TOKEN) lexer->yylex();
	ftype=Expression();
	cout << "\tpop %rcx\t\t# mettre l'element a comparer dans rdx"<<endl;
	cout << "\tcmpq %rdx,%rcx\t\t# le comparer avec la valeur qu'on avait stocké dans rbx"<<endl;
	cout << "\tje CASEstatement"<<tag<<"\t\t# s'elles sont egales on excute l'instruction du CaseListElement "<<endl;
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		stype=Expression();
		cout << "\tpop %rcx\t\t# mettre l'element a comparer dans rdx"<<endl;
		cout << "\tcmpq %rdx,%rcx\t\t# le comparer avec la valeur qu'on avait stocké dans rbx"<<endl;
		cout << "\tje CASEstatement"<<tag<<"\t\t# s'elles sont egales on excute l'instruction du CaseListElement "<<endl;
		if(stype!=ftype) Error("CaseLabelList : valeurs pas du meme type;");
	}
	cout << "\tjmp FinCASEstatement"<<tag<<"\t\t# PAS EGALES - on va au prochain CaseListElement"<<endl;
	return ftype;
}
//<case list element> ::= <case label list> : <statement>
TYPES CaseListElement(unsigned long long tag,unsigned long long cas){
	TYPES ftype;
	ftype=CaseLabelList(tag);
	if(current==COLON){
		cout<<"CASEstatement"<<tag<<":"<<"\t\t#Debut de l'instruction de ce CaseListElement"<<endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		cout<<"jmp FinCASE"<<cas<<"\t\t#si l'instruction a été executé pas besoin de voir les autres, aller à la fin du case"<<endl;
		cout<<"FinCASEstatement"<<tag<<":"<<"\t\t#Fin de ce CaseListElement, on passe au suivant"<<endl;
	}
	else
		Error("COLON expected");
	return ftype;
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	if(!strcmp(lexer->YYText(),"BEGIN")){
		current=(TOKEN) lexer->yylex();
		Statement();
		while(current==SEMICOLON){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(current!=KEYWORD||strcmp(lexer->YYText(), "END")!=0)
			Error("END expected");
		current=(TOKEN) lexer->yylex();
	}
	else Error("BEGIN expected");
}

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
void ForStatement(void){
	TYPES ftype, stype;
	unsigned long long tag=TagNumber++;
	cout<<"ForInit"<<tag<<":"<<endl;

	if(!strcmp(lexer->YYText(),"FOR")){
		current=(TOKEN) lexer->yylex();
		AssignementStatement();

		if(!strcmp(lexer->YYText(),"To")){
			cout<<"For"<<tag<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			ftype=Expression();
			stype=typ[cptt-1];

			if(ftype!=stype) Error("probleme de variable entre l'initialisation et l'expression");
			cout<<"\tpop %rax\t"<<endl;
			cout<<"\tcmpq "<<var[cptt-1]<<", %rax\t"<<endl; // comparation variable/resultat
			cout<<"\tjnae ForEND"<<tag<<"\t"<<endl;

			if(!strcmp(lexer->YYText(),"DO")){

				if(stype==DOUBLE){

					cout<<"\tpush "<<var[cptt-1]<<endl;
					cout<<"\tpush ForDoubleIncrementation"<<endl;
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t"<<endl;
					cout<<"\tpop "<<var[cptt-1]<<endl;
				}
				else cout<<"\taddq $1,"<<var[cptt-1]<<endl;

				current=(TOKEN) lexer->yylex();
				Statement();
				cptt--;
				cout<<"\tjmp For"<<tag<<endl;
				cout<<"ForEND"<<tag<<":"<<endl;

			}else Error("DO expected");
		}else Error("To expected");
	}else Error("FOR expected");
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"While"<<tag<<":"<<endl;
	if(!strcmp(lexer->YYText(),"WHILE")){
		current=(TOKEN) lexer->yylex();
		if(Expression()!=BOOLEAN) Error("expression non boolean");
		cout<<"\tpop %rax\t"<<endl;
		cout<<"\tcmpq $0, %rax\t"<<endl; //comparation du resultat et 0
		cout<<"\tje EndWhile"<<tag<<"\t"<<endl; //sortie du while
		if(current!=KEYWORD||strcmp(lexer->YYText(), "DO")!=0)
			Error("DO expected");
		current=(TOKEN) lexer->yylex();
		Statement();
		cout<<"\tjmp While"<<tag<<endl;
		cout<<"EndWhile"<<tag<<":"<<endl;
	}else Error("WHILE expected");
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	//un peu la meme chose que le while
	TYPES ftype;
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"IF")){
		current=(TOKEN) lexer->yylex();
		ftype=Expression();
		if(ftype!=BOOLEAN) Error("expression non boolean");
		cout<<"\tpop %rax\t"<<endl;
		cout<<"\tcmpq $0, %rax\t"<<endl;
		cout<<"\tje Else"<<tag<<"\t"<<endl;
		if(!strcmp(lexer->YYText(),"THEN")){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout<<"\tjmp Next"<<tag<<"\t"<<endl;
			cout<<"\tElse"<<tag<<":"<<endl;
			if(!strcmp(lexer->YYText(),"ELSE")){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
			cout<<"Next"<<tag<<":"<<endl;
		}else Error("THEN expected");
	}else Error("IF expected");
}

// Display := "DISPLAY" Expression
void Display(void){
	TYPES type;
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	if(type==INTEGER){
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}
	else if(type==BOOLEAN){
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tcall	puts@PLT"<<endl;
	}
	else if(type==DOUBLE){
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
	}
	else if(type==CHAR){
		cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
		cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}else
		Error("DISPLAY non disponible pour ce type.");
}

// ProcedureDeclaration := <NomFonction> ";" BlockStatement
void ProcedureDeclaration(void){
	string nom;
	current=(TOKEN) lexer->yylex();
	if(current!=ID) Error("ID expected.");
	nom=lexer->YYText();
	fon[ind]=nom;
	cout<<fon[ind]<<":"<<endl;
	ind++;
	current=(TOKEN) lexer->yylex();
	if(current!=SEMICOLON) Error("';' expected.");
	current=(TOKEN) lexer->yylex();
	BlockStatement();
	cout<<"\tret"<<endl;
}

void ProcedureCall(int i){
	cout<<"\tcall "<<fon[i]<<endl;
	current=(TOKEN) lexer->yylex();
}

// AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | Display | Case | Procedure
void Statement(void){
	if(current==ID){
		bool assignement=true;
		for(int i=0;i<ind;i++){
			if(fon[i]==lexer->YYText()){
				assignement=false;
				ProcedureCall(i);
				break;
			}
		}
		if(assignement==true) AssignementStatement();
	} 
	else if(current==KEYWORD){
		if(!strcmp(lexer->YYText(),"WHILE")){
			WhileStatement();
		}
		else if(!strcmp(lexer->YYText(),"IF")){
			IfStatement();
		}
		else if(!strcmp(lexer->YYText(),"FOR")){
			ForStatement();
		}
		else if(!strcmp(lexer->YYText(),"BEGIN")){
			BlockStatement();
		}
		else if(!strcmp(lexer->YYText(),"DISPLAY")){
			Display();
		}
		else if(!strcmp(lexer->YYText(),"PROCEDURE")){
			Error("PROCEDURE doit etre avant le main");
		}
	}
	else Error("ID ou KEYWORD expected");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.align 8"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' expected");
	current=(TOKEN) lexer->yylex();
}

// Program := [VarDeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(), "VAR")==0)
		VarDeclarationPart();
	while((!strcmp(lexer->YYText(),"PROCEDURE"))){
		ProcedureDeclaration();
		current=(TOKEN) lexer->yylex();
	}
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 
	cout << "ForDoubleIncrementation: .double 1.0"<<endl;
	cout << "ForRadiant: .double 0.01745392\t\t#=(pigreque/180)"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"erreur fin de programme : "<<current;
		Error("."); // unexpected characters at the end of program
	}

}
